﻿using System;
using System.Data;

namespace TrueClarity.MovieTickets.Website.Queries
{
    public abstract class QueryBase
    {
        private readonly IDbConnection connection;

        protected QueryBase(IDbConnection connection)
        {
            this.connection = connection;
        }

        protected IDataReader ExecuteReader(string commandText, params IDbDataParameter[] args)
        {
            var cmd = this.connection.CreateCommand();
            cmd.CommandText = commandText;
            cmd.CommandType = CommandType.Text;
            Array.ForEach(args, arg => cmd.Parameters.Add(arg));

            return cmd.ExecuteReader();
        }
    }
}
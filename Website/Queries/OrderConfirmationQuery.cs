﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using TrueClarity.MovieTickets.Website.Models;

namespace TrueClarity.MovieTickets.Website.Queries
{
    public class OrderConfirmationQuery : QueryBase
    {
        public OrderConfirmationQuery(IDbConnection connection)
            : base(connection)
        {
        }

        public OrderConfirmation Execute(Guid orderId)
        {
            const string DetailsSql = "SELECT * FROM [Order] WHERE Id = @id";

            OrderDetail detail = null;
            using (var rdr = this.ExecuteReader(DetailsSql, new SqlParameter("@id", orderId)))
            {
                rdr.Read();
                var name = ReadString(rdr, "Name");
                var email = ReadString(rdr, "Email");
                var cardNumber = ReadString(rdr, "CardNumber");
                var cardType = ReadString(rdr, "CardType");
                var optIn = ReadBoolean(rdr, "OptIn");
                detail = new OrderDetail
                             {
                                 Name = name,
                                 Email = email,
                                 CreditCardNumber = cardNumber,
                                 CreditCardType = cardType,
                                 OptIn = optIn
                             };
            }

            var items = new List<OrderItem>();
            const string ItemsSql = "SELECT * FROM [OrderItem] WHERE OrderId = @orderId";
            using (var rdr = this.ExecuteReader(ItemsSql, new SqlParameter("@orderId", orderId)))
            {
                rdr.Read();
                var title = ReadString(rdr, "Title");
                var quantity = ReadInteger(rdr, "Qty");
                var price = ReadMoney(rdr, "Price");
                items.Add(new OrderItem { Price = price, Qty = quantity, Title = title });
            }

            return new OrderConfirmation { Detail = detail, Items = items };
        }

        private static string ReadString(IDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return reader.GetString(ordinal);
        }

        private static bool ReadBoolean(IDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return reader.GetBoolean(ordinal);
        }

        private static int ReadInteger(IDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return reader.GetInt32(ordinal);
        }

        private static double ReadMoney(IDataReader reader, string fieldName)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return (double)reader.GetDecimal(ordinal);
        }
    }
}
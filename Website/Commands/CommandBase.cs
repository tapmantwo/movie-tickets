﻿namespace TrueClarity.MovieTickets.Website.Commands
{
    using System;
    using System.Data;

    public abstract class CommandBase
    {
        private readonly IDbConnection connection;

        protected CommandBase(IDbConnection connection)
        {
            this.connection = connection;
        }

        protected void ExecuteNonQuery(string commandText, params IDbDataParameter[] args)
        {
            var cmd = this.connection.CreateCommand();
            cmd.CommandText = commandText;
            cmd.CommandType = CommandType.Text;
            Array.ForEach(args, arg => cmd.Parameters.Add(arg));

            cmd.ExecuteNonQuery();
        }
    }
}
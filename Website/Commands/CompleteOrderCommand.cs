﻿namespace TrueClarity.MovieTickets.Website.Commands
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using TrueClarity.MovieTickets.Website.Models;

    public class CompleteOrderCommand : CommandBase
    {
        public CompleteOrderCommand(IDbConnection connection)
            : base(connection)
        {
        }

        public void Execute(Guid orderId, OrderDetail orderDetails)
        {
            const string CompleteOrderSql = "UPDATE [ORDER] SET Name = @name, Email = @email, CardNumber = @cardNumber, CardType = @cardType, OptIn = @optIn WHERE Id = @orderId";
            this.ExecuteNonQuery(CompleteOrderSql, new SqlParameter("@name", orderDetails.Name), new SqlParameter("@email", orderDetails.Email), new SqlParameter("@cardNumber", orderDetails.CreditCardNumber), new SqlParameter("@cardType", orderDetails.CreditCardType), new SqlParameter("@optIn", orderDetails.OptIn), new SqlParameter("@orderId", orderId));
        }
    }
}
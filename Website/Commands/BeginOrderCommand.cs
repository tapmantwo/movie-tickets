﻿namespace TrueClarity.MovieTickets.Website.Commands
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;

    using TrueClarity.MovieTickets.Website.Models;

    public class BeginOrderCommand : CommandBase
    {
        public BeginOrderCommand(IDbConnection connection)
            : base(connection)
        {
        }

        public void Execute(Guid orderId, IEnumerable<OrderItem> items)
        {
            const string CreateOrderSql = "INSERT INTO [Order] (Id) VALUES (@id)";
            this.ExecuteNonQuery(CreateOrderSql, new SqlParameter("@id", orderId));

            const string CreateOrderItemSql = "INSERT INTO OrderItem (Title, Price, Qty, OrderId) VALUES (@title, @price, @qty, @orderId)";
            foreach (var item in items)
            {
                this.ExecuteNonQuery(CreateOrderItemSql, new SqlParameter("@orderId", orderId), new SqlParameter("@title", item.Title), new SqlParameter("@price", item.Price), new SqlParameter("@qty", item.Qty));
            }
        }
    }
}
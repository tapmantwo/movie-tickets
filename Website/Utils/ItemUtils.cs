﻿using Sitecore.Shell.Applications.ContentEditor;

namespace TrueClarity.MovieTickets.Website.Utils
{
    public static class ItemUtils
    {
        public static string GetUrl(this Sitecore.Data.Items.Item item)
        {
            var urlOptions = (Sitecore.Links.UrlOptions)Sitecore.Links.UrlOptions.DefaultOptions.Clone();
            urlOptions.LowercaseUrls = true;
            return Sitecore.Links.LinkManager.GetItemUrl(item, urlOptions);
        }
    }
}
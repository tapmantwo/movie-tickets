﻿using System;
using System.Data;

namespace TrueClarity.MovieTickets.Website.Utils
{
    public static class DatabaseUtils
    {
        public static void OpenAndThen(this System.Configuration.ConnectionStringSettings connectionString, Action<IDbConnection> action)
        {
            using (var connection = new System.Data.SqlClient.SqlConnection(connectionString.ConnectionString))
            {
                connection.Open();

                action.Invoke(connection);

                connection.Close();
            }
        }
    }
}
﻿using System.Web.UI;

namespace TrueClarity.MovieTickets.Website.Utils
{
    public static class ControlUtils
    {
        public static Control FindControlIn(Control control, string id)
        {
            if (control.ID == id)
            {
                return control;
            }

            var found = control.FindControl(id);
            if (found != null)
            {
                return found;
            }

            foreach (Control child in control.Controls)
            {
                found = FindControlIn(child, id);
                if (found != null)
                {
                    return found;
                }
            }

            return null;
        }
    }
}
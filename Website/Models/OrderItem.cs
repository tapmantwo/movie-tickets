﻿namespace TrueClarity.MovieTickets.Website.Models
{
    public class OrderItem
    {
        public string Title { get; set; }

        public double Price { get; set; }

        public int Qty { get; set; }
    }
}
﻿using System.Collections.Generic;

namespace TrueClarity.MovieTickets.Website.Models
{
    public class OrderConfirmation
    {
        public OrderDetail Detail { get; set; }

        public IEnumerable<OrderItem> Items { get; set; }
    }
}
﻿namespace TrueClarity.MovieTickets.Website.Models
{
    public class OrderDetail
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string CreditCardNumber { get; set; }

        public string CreditCardType { get; set; }

        public bool OptIn { get; set; }
    }
}
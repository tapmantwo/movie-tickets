﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MovieDetail.ascx.cs" Inherits="TrueClarity.MovieTickets.Website.Layouts.MovieDetail" %>
<div class="ui form segment left aligned">
    <h2 class="ui black header">
        <sc:Text ID="Text1" DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.MovieDetailLabels %>' Field="Header" runat="server"/>
    </h2>
    <h2 class="ui black block left aligned header">
        <sc:Text DataSource="<%#Sitecore.Context.Item.ID%>" Field="Title" runat="server"/>
    </h2>
    <div class="ui segment left aligned">
        <sc:Text DataSource="<%#Sitecore.Context.Item.ID%>" Field="Description" runat="server"/>
    </div>
    <div class="ui">
        <a href="/" class="ui orange button">
            <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.MovieDetailLabels %>' Field="BackButton" runat="server"/>
        </a>
    </div>
</div>
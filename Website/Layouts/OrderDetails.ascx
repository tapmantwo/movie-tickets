﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderDetails.ascx.cs" Inherits="TrueClarity.MovieTickets.Website.Layouts.OrderDetails" %>
<div class="ui fluid form segment">
    <div class="ui three column grid">
        <div class="row">
            <div class="column right aligned">
                <label for="nameTextBox">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Name" runat="server"/>
                </label>
            </div>
            <div class="two wide column">
                <asp:TextBox runat="server" ID="nameTextBox" ClientIDMode="Static" />
            </div>
        </div>

        <div class="row">
            <div class="column right aligned">
                <label for="emailTextBox">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Email" runat="server"/>
                </label>
            </div>
            <div class="two wide column">
                <asp:TextBox runat="server" ID="emailTextBox" TextMode="Email" ClientIDMode="Static" />
            </div>
        </div>

        <div class="row">
            <div class="column right aligned">
                <label for="cardNumberTextBox">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Credit card number" runat="server"/>
                </label>
            </div>
            <div class="two wide column">
                <asp:TextBox runat="server" ID="cardNumberTextBox" MaxLength="16" TextMode="Number" ClientIDMode="Static"/>
            </div>
        </div>
    
        <div class="row">
            <div class="column right aligned">
                <label for="cardTypeDropDown">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Credit card type" runat="server"/>
                </label>
            </div>
            <div class="two wide column">
                <asp:DropDownList ID="cardTypeDropDown" runat="server" DataTextField="Name" DataValueField="Key" CssClass="ui selection dropdown" ClientIDMode="Static" />
            </div>
        </div>

        <div class="row">
            <div class="column right aligned">
                <label for="optInCheckBox">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Email optin" runat="server"/>                    
                </label>
            </div>
            <div class="two wide column">
                <asp:CheckBox runat="server" ID="optInCheckBox" CssClass="ui toggle checkbox" Text="&nbsp" ClientIDMode="Static"/>
            </div>
        </div>
 
        <div class="row">
            <div class="right floated three wide column">
                <asp:LinkButton CssClass="ui orange button" ID="submitButton" runat="server" OnClick="SubmitClick">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.OrderDetailsLabels%>' Field="Submit button" runat="server"/>
                </asp:LinkButton>
            </div>
        </div>
    </div>
</div>
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Sitecore.Web.UI.WebControls;

namespace TrueClarity.MovieTickets.Website.Layouts
{
    public partial class MovieDetail : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
               this.DataBind();
            }
        }
    }
}
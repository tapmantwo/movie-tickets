using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;

using Sitecore.Web.UI.WebControls;

using TrueClarity.MovieTickets.Website.Commands;
using TrueClarity.MovieTickets.Website.Models;
using TrueClarity.MovieTickets.Website.Utils;

namespace TrueClarity.MovieTickets.Website.Layouts
{
    public partial class MovieBasket : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected override void OnInit(EventArgs e)
        {
            var sublayout = this.Parent as Sublayout;
            if (sublayout != null)
            {
                var ds = sublayout.DataSource;
                movieRepeater.DataSource = Sitecore.Context.Database.GetItem(ds).Children;
            }

            movieRepeater.DataBind();
            this.DataBind();
        }

        protected void UpdateClick(object sender, EventArgs e)
        {
            var total = 0.0d;
            foreach (RepeaterItem item in movieRepeater.Items)
            {
                var textBox = item.FindControl("quantityTextBox") as TextBox;
                if (textBox != null)
                {
                    var qtyText = textBox.Text;
                    var qty = 0;
                    if (int.TryParse(qtyText, out qty) && qty > 0)
                    {
                        var hiddenField = ControlUtils.FindControlIn(item, "itemPriceHiddenField") as HiddenField;
                        if (hiddenField != null)
                        {
                            var price = double.Parse(hiddenField.Value);
                            total += price * qty;
                        }
                    }
                }
            }

            totalLabel.Text = total.ToString("C");
            orderButton.Enabled = total > 0;
        }

        protected void OrderClick(object sender, EventArgs e)
        {
            var items = new List<OrderItem>();
            foreach (RepeaterItem item in movieRepeater.Items)
            {
                var textBox = item.FindControl("quantityTextBox") as TextBox;
                if (textBox != null)
                {
                    var qtyText = textBox.Text;
                    var qty = 0;
                    if (int.TryParse(qtyText, out qty) && qty > 0)
                    {
                        var hiddenField = ControlUtils.FindControlIn(item, "itemPriceHiddenField") as HiddenField;
                        if (hiddenField != null)
                        {
                            var price = double.Parse(hiddenField.Value);
                            var field = ControlUtils.FindControlIn(item, "itemTitleHiddenField") as HiddenField;
                            if (field != null)
                            {
                                var title = field.Value;
                                items.Add(new OrderItem
                                              {
                                                  Price = price,
                                                  Qty = qty,
                                                  Title = title
                                              });
                            }
                        }
                    }
                }
            }

            var orderId = Guid.NewGuid();
            var connectionSetting = ConfigurationManager.ConnectionStrings["order"];
            connectionSetting.OpenAndThen(connection =>
            {
                var command = new BeginOrderCommand(connection);
                command.Execute(orderId, items);
            });

            this.Response.Redirect("/OrderDetails?oid=" + orderId.ToString());
        }
    }
}
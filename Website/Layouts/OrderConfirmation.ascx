﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="OrderConfirmation.ascx.cs" Inherits="TrueClarity.MovieTickets.Website.Layouts.OrderConfirmation" %>
<div class="ui fluid form segment">
    <h2 class="ui left aligned dividing header">
        <sc:Text ID="Text1" DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Order Details" runat="server"/>
    </h2>
    <div class="ui three column grid">
        <div class="row">
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Name" runat="server"/>
            </div>
            <div class="two wide column">
                <asp:Literal runat="server" ID="name" />
            </div>
        </div>
        <div class="row">
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Email" runat="server"/>
            </div>
            <div class="two wide column">
                <asp:Literal runat="server" ID="email" />
            </div>
        </div>
        <div class="row">
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Credit card number" runat="server"/>
            </div>
            <div class="two wide column">
                <asp:Literal runat="server" ID="cardNumber"/>
            </div>
        </div>
        <div class="row">
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Credit card type" runat="server"/>
            </div>
            <div class="two wide column">
                <asp:Literal runat="server" ID="cardType" />
            </div>
        </div>
        <div class="row">
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Email optin" runat="server"/>
            </div>
            <div class="two wide column">
                <asp:Literal runat="server" ID="optIn" />
            </div>
        </div>
    </div>
    <h2 class="ui left aligned dividing header">
        <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Tickets" runat="server"/>
    </h2>
    <div class="ui two column grid">
        <asp:Repeater ID="tickets" runat="server">
            <ItemTemplate>
                <div class="row">
                    <div class="column">
                        <asp:Literal id="title" Text='<%#Eval("Title")%>' runat="server" />
                    </div>
                    <div class="column">
                        <asp:Literal id="quantity" Text='<%#Eval("Qty")%>' runat="server" />
                         x 
                        <asp:Label id="price" Text='<%#Eval("Price", "{0:C}")%>' runat="server" />
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <div class="row">
            <div class="column right aligned">
                <strong>
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.ConfirmationDetailsLabels%>' Field="Total" runat="server"/>
                </strong>
            </div>
            <div class="column"><strong><asp:Literal id="total" runat="server" /></strong></div>
        </div>
    </div>
</div>
﻿using System;
using System.Configuration;
using System.Linq;

using TrueClarity.MovieTickets.Website.Configuration;
using TrueClarity.MovieTickets.Website.Queries;
using TrueClarity.MovieTickets.Website.Utils;

namespace TrueClarity.MovieTickets.Website.Layouts
{
    public partial class OrderConfirmation : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.IsPostBack)
            {
                var orderId = Guid.Parse(this.Request.QueryString["oid"]);

                var connectionSetting = ConfigurationManager.ConnectionStrings["order"];
                connectionSetting.OpenAndThen(connection =>
                {
                    var query = new OrderConfirmationQuery(connection);
                    var model = query.Execute(orderId);

                    this.BindForm(model);
                });

                this.DataBind();
            }
        }

        private static string GetCreditCardTypeName(string key)
        {
            var allCards = Sitecore.Context.Database.GetItem(Ids.Content.CreditCardTypes);
            var matching = allCards.GetChildren().First(c => c.Fields["Key"].Value == key);
            return matching.Fields["Name"].Value;
        }

        private static string Mask(string value)
        {
            if (value.Length < 4)
            {
                return value;
            }

            return value.Substring(0, 4) + " **** **** ****";
        }

        private void BindForm(Models.OrderConfirmation model)
        {
            name.Text = model.Detail.Name;
            email.Text = model.Detail.Email;
            cardNumber.Text = Mask(model.Detail.CreditCardNumber);
            cardType.Text = GetCreditCardTypeName(model.Detail.CreditCardType);
            optIn.Text = model.Detail.OptIn ? "Yes" : "No";
            tickets.DataSource = model.Items;
            tickets.DataBind();
            total.Text = model.Items.Sum(o => o.Qty * o.Price).ToString("C");
        }
    }
}
﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="MovieBasket.ascx.cs" Inherits="TrueClarity.MovieTickets.Website.Layouts.MovieBasket" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="TrueClarity.MovieTickets.Website.Utils" %>
<div class="ui fluid form segment">
    <div class="ui three column grid">
        <div class="row ui header">
            <div class="column">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="TitleLabel" runat="server"/>
            </div>
            <div class="column">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="QuantityLabel" runat="server"/>
            </div>
            <div class="column">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="PriceLabel" runat="server"/>
            </div>
        </div>

        <asp:Repeater id="movieRepeater" runat="server">
            <ItemTemplate>
                        <div class="row">
                            <div class="column">
                                <asp:HiddenField ID="itemTitleHiddenField" Value='<%# ((Item)Container.DataItem).Fields["Title"] %>' runat="server" /> 
                                <a href="<%#((Item)Container.DataItem).GetUrl()%>" class="small ui orange button">
                                    <sc:Text ID="Title" Item="<%# ((Item)Container.DataItem) %>" Field="Title" runat="server" />
                                </a>
                            </div>
                            <div class="column"><asp:TextBox id="quantityTextBox" TextMode="Number" runat="server" /></div>
                            <div class="column">
                                <asp:HiddenField ID="itemPriceHiddenField" Value='<%# ((Item)Container.DataItem).Fields["Price"] %>' runat="server" /> 
                                <sc:Text ID="Price" Item="<%# ((Item)Container.DataItem) %>" Field="Price" runat="server"   />
                            </div>
                        </div>
            </ItemTemplate>
        </asp:Repeater>

        <div class="row">
            <div class="column">&nbsp;</div>
            <div class="column right aligned">
                <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="Total" runat="server"/>
            </div>
            <div class="column"><asp:Label ID="totalLabel" runat="server" Text="£0.00"/></div>
        </div>
        <div class="row">
            <div class="right floated three wide column">
                <asp:LinkButton CssClass="ui orange button" ID="updateButton" runat="server" OnClick="UpdateClick">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="UpdateButton" runat="server"/>
                </asp:LinkButton>
            </div>
        </div>
    </div>

    <div class="ui yellow message">
        <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="ValidityMessage" runat="server"/>
    </div>

    <div class="ui three column grid">
        <div class="row">
            <div class="right floated three wide column">
                <asp:LinkButton CssClass="ui orange button" ID="orderButton" runat="server" OnClick="OrderClick" Enabled="false">
                    <sc:Text DataSource='<%#TrueClarity.MovieTickets.Website.Configuration.Ids.Content.BasketHeaderLabels %>' Field="ContinueButton" runat="server"/>
                </asp:LinkButton>
            </div>
        </div>
    </div>
</div>

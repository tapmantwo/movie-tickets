﻿using System;
using System.Configuration;
using System.Web.UI.WebControls;

using Sitecore.Data.Items;
using Sitecore.Web.UI.WebControls;

using TrueClarity.MovieTickets.Website.Commands;
using TrueClarity.MovieTickets.Website.Models;
using TrueClarity.MovieTickets.Website.Utils;

namespace TrueClarity.MovieTickets.Website.Layouts
{
    public partial class OrderDetails : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.DataBind();
            }
        }

        protected override void OnInit(EventArgs e)
        {
            var sublayout = this.Parent as Sublayout;
            if (sublayout != null)
            {
                var ds = sublayout.DataSource;

                cardTypeDropDown.Items.Clear();
                foreach (Item item in Sitecore.Context.Database.GetItem(ds).Children)
                {
                    cardTypeDropDown.Items.Add(new ListItem(item.Fields["Name"].Value, item.Fields["Key"].Value));
                }
            }
        }

        protected void SubmitClick(object sender, EventArgs e)
        {
            var order = new OrderDetail
                            {
                                Name = nameTextBox.Text,
                                Email = emailTextBox.Text,
                                CreditCardNumber = cardNumberTextBox.Text,
                                CreditCardType = cardTypeDropDown.SelectedValue,
                                OptIn = optInCheckBox.Checked
                            };

            var orderId = Guid.Parse(this.Request.QueryString["oid"]);
            var connectionSetting = ConfigurationManager.ConnectionStrings["order"];
            connectionSetting.OpenAndThen(
                connection =>
                    {
                        var command = new CompleteOrderCommand(connection);
                        command.Execute(orderId, order);
                    });

            Response.Redirect("/OrderComplete?oid=" + orderId.ToString());
        }
    }
}
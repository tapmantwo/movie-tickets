﻿namespace TrueClarity.MovieTickets.Website.Configuration
{
    public static class Ids
    {
        public static class Content
        {
            public const string BasketHeaderLabels = "FC5705B7-7ACC-49AA-A34F-5E31C03C8DF3";

            public const string OrderDetailsLabels = "18F8D0B8-FA0F-4C17-9A73-01EB8752393A";

            public const string ConfirmationDetailsLabels = "EF5D1FF6-2BB5-4E36-960B-DB46AB8FB9C8";

            public const string MovieDetailLabels = "3A7AC313-4B75-4439-90B8-CBF14294A6C5";

            public const string CreditCardTypes = "48A0DF04-23D2-4EA8-B5FB-8EACB001B22F";
        }
    }
}